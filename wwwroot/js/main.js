let todos = [];
let listContainter = document.getElementById('item-list');
let addInput = document.getElementById('item');
let xhttp = new XMLHttpRequest();

function loadToDos() {
    todos = []
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            todos = JSON.parse(xhttp.responseText);
            renderTodos();
        }
    };
    xhttp.open("GET", "all", true);
    xhttp.send();
}
loadToDos();

function addItem(item) {
    listContainter.innerHTML +=
        `<li class="item" id="item-${item}">
            <input class="item-text" id="input-${item}" name="${item}" value="${item}" disabled="disabled">
            <img class="item-icon" src="img/rubbish-bin.svg" alt="delete icon" onclick="deleteToDo('${item}')">
            <img class="item-icon" src="img/edit.svg" alt="edit icon" onclick="editToDo('${item}')">
        </li>`;
}

function deleteToDo(item) {
    todos = todos.filter((val) => val != item);
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            loadToDos();
        }
    };
    xhttp.open("DELETE", item, true);
    xhttp.send();
}

function renderTodos() {
    listContainter.innerHTML = '';
    for (let i of todos) {
        addItem(i.name);
    }
}
renderTodos();

function addToDo(event) {
    event.preventDefault();
    let val = addInput.value;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            loadToDos();
        }
    };
    xhttp.open("POST", "new/" + val, true);
    if (val != "") {
        xhttp.send();
        addInput.value = null;
    }
}


function editToDo(item) {
    let field = document.getElementById("input-" + item);
    field.removeAttribute('disabled');
    field.focus();
    field.addEventListener('focusout', () => {
        xhttp.onreadystatechange = function () { 
            if (this.readyState == 4 && this.status == 200) {
                loadToDos();
            }
        };
        xhttp.open("PUT", "" + item + "/" + field.value, true);
        xhttp.send();
    });
}