using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoAppA.Models;

namespace ToDoAppA.Controllers
{
    [Route("")]
    [ApiController]
    public class ToDoController : ControllerBase
    {
        // GET /
        [HttpGet]
        public ActionResult Get()
        {
            return Redirect("/index.html");
        }

        // GET all
        [HttpGet("all")]
        public ActionResult<List<ToDo>> All()
        {
            // using (var db = new ToDoContext())
            // {
            return ToDoContext.ToDos.ToList();
            // }
        }

        // POST new
        [HttpPost("{value}")]
        public void Post(string value)
        {
            // using (var db = new ToDoContext())
            // {
            ToDoContext.ToDos.Add(new ToDo() { Name = value, Id = ToDoContext.count++ });
            //db.SaveChanges();
            // }
        }

        // Delete /
        [HttpDelete("{name}")]
        public void Delete(string name)
        {
            // using (var db = new ToDoContext())
            // {
            ToDoContext.ToDos.Remove(ToDoContext.ToDos.Where(x => x.Name == name).First());
            //db.SaveChanges();
            // }
        }

        // PUT /
        [HttpPut("{id}/{nn}")]
        public void Put(int id, string nn)
        {
            // using (var db = new ToDoContext())
            // {
            ToDoContext.ToDos[ToDoContext.ToDos.FindIndex(0, ToDoContext.ToDos.Count, x => x.Id == id)].Name = nn;
            //db.SaveChanges();
            // }
        }
    }
}
