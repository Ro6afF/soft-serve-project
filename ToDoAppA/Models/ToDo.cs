using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAppA.Models
{
    // public class ToDoContext : DbContext
    // {
    //     public DbSet<ToDo> ToDos { get; set; }

    //     protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //     {
    //         optionsBuilder.UseSqlServer(
    //             @"Server=(LocalDb)\MSSQLLocalDB;Initial Catalog=wiiii;Integrated Security=True;AttachDBFilename=AAAA\fakU.mdf");
    //     }
    // }

    public class ToDoContext
    {
        public static int count = 2;
        public static List<ToDo> ToDos = new List<ToDo>() {new ToDo() {Id=0, Name="aaaa"}, new ToDo() {Id = 1, Name = "bbbbb"}};
    }

    public class ToDo
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
