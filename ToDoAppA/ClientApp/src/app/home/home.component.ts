import { Component, OnInit } from '@angular/core';
import { ToDo } from '../todo';
import { ToDoService } from './home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  todos = [{ id: 123, name: "1234" }];
  inputVal = "";
  disabled = {};
  constructor(private todoService: ToDoService) {
    this.getTodos();
  }

  ngOnInit() {
    console.log("sadf")
    this.getTodos();
  }

  onAdd(): void {
    this.todoService.addToDo(this.inputVal).subscribe(_ => this.getTodos());
    this.inputVal = "";
  }

  onKeyUp(e: KeyboardEvent): void {
    if (e.key == "Enter") {
      this.onAdd();
    }
  }

  getTodos(): void {
    this.todoService.getToDos()
      .subscribe(todos => {
        this.todos = todos;
        for (let i of this.todos) {
          this.disabled[i.id] = true;
        }
        console.log(this.disabled);
      }
      );
  }

  onDelete(todo: ToDo): void {
    this.todoService.deleteToDo(todo).subscribe(_ => this.getTodos());
  }

  toggleEdit(id: number): void {
    if (!this.disabled[id]) {
      this.todoService.updateToDo(this.todos.filter(x => x.id == id)[0]).subscribe(_ => this.getTodos());
    }
    this.disabled[id] = !this.disabled[id];
  }
}
