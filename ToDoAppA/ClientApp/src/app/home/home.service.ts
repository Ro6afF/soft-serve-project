import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { ToDo } from "../todo";

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ToDoService {
    private heroesUrl = '';
    constructor(private http: HttpClient) { }

    getToDos(): Observable<ToDo[]> {
        return this.http.get<ToDo[]>(this.heroesUrl + '/all').pipe();
    }

    addToDo(todo: string): Observable<ToDo> {
        return this.http.post<ToDo>(this.heroesUrl + '/' + todo, httpOptions).pipe();
    }

    deleteToDo(todo: ToDo | string): Observable<ToDo> {
        const name = typeof todo === 'string' ? todo : todo.name;
        const url = `${this.heroesUrl}/${name}`;
        return this.http.delete<ToDo>(url, httpOptions).pipe();
    }

    updateToDo(todo: ToDo): Observable<any> {
        return this.http.put(this.heroesUrl + '/' + todo.id + '/' + todo.name, httpOptions).pipe();
    }
}